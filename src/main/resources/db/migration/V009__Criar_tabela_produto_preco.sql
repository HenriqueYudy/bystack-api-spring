CREATE TABLE produto_preco(
	id_produto_preco 	SERIAL 			PRIMARY KEY,
	data_inicio		    Date			NOT NULL,
	data_fim			Date			,
	preco			    decimal			NOT NULL,
	produto				bigint			NOT NULL,
	constraint fk_produto_preco_produto foreign key (produto) references produto (id_produto)
);

insert into produto_preco values (1 , '2019-06-26', null , 50.00, 1);
insert into produto_preco values (2 , '2019-06-26', null , 500.00, 2);
insert into produto_preco values (3 , '2019-06-26', null , 1500.00, 3);
insert into produto_preco values (4 , '2019-06-26', null , 1660.00, 4);
insert into produto_preco values (5 , '2019-06-26', '2019-06-28' , 1660.00, 4);