CREATE TABLE nota_fiscal_entrada(
	id_nota_fiscal_entrada 			SERIAL 			PRIMARY KEY,
	id_movimentacao_entrada		    bigint			NOT NULL,
	data_nota_fiscal				Date			NOT NULL,
	valor			   				DECIMAL			NOT NULL,
	constraint fk_nota_entrada_mov foreign key (id_movimentacao_entrada) references movimentacao_entrada (id_movimentacao_entrada)
)
