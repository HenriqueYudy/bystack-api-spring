package com.projeto.bystack.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projeto.bystack.domain.model.MovimentacaoEntrada;

@Repository
public interface MovimentacaoEntradaRepository extends JpaRepository<MovimentacaoEntrada, Long> {

}
