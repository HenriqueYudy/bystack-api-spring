package com.projeto.bystack.domain.repository.Query;

import com.projeto.bystack.domain.model.Cliente;

public interface ClienteRepositoryQuery {
	
	public Cliente buscarPorCpf(String cpf);

}
