package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.Categoria;
import com.projeto.bystack.domain.repository.CategoriaRepository;

@RestController
@RequestMapping(value = "/categoria")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Categoria> saveCategoria(@Valid @RequestBody Categoria categoria) {

		Categoria newCategoria = categoriaRepository.save(categoria);

		return ResponseEntity.status(HttpStatus.CREATED).body(newCategoria);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Categoria> getCategoria(@PathVariable("id") long id) {

		Categoria resultCategoria = categoriaRepository.findOne(id);

		if (resultCategoria == null) {
			return new ResponseEntity<Categoria>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Categoria>(resultCategoria, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Categoria>> getAllCategoria() {

		List<Categoria> AllCategoria = categoriaRepository.findAll();

		if (AllCategoria.isEmpty()) {
			return new ResponseEntity<List<Categoria>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Categoria>>(AllCategoria, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Categoria> updateCategoria(@PathVariable("id") long id, @RequestBody Categoria categoria) {

		Categoria currentCategoria = categoriaRepository.findOne(id);
		if (currentCategoria == null) {
			return new ResponseEntity<Categoria>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(categoria, currentCategoria, "id");
		categoriaRepository.save(currentCategoria);
		return new ResponseEntity<Categoria>(categoria, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path =  "/delete/{id}") 
	public ResponseEntity<Categoria> deleteCategoria(@PathVariable("id") long id) {

		Categoria deleteCategoria = categoriaRepository.findOne(id);

		if (deleteCategoria == null) {
			return new ResponseEntity<Categoria>(HttpStatus.NOT_FOUND);
		}

		categoriaRepository.delete(deleteCategoria);
		return new ResponseEntity<Categoria>(HttpStatus.NO_CONTENT);

	}

}
