package com.projeto.bystack.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.NotaFiscalEntrada;
import com.projeto.bystack.domain.repository.NotaFiscalEntradaRepository;

@RestController
@RequestMapping(value = "/nota_fiscal_entrada")
public class NotaFiscalEntradaResource {
	
	@Autowired
	private NotaFiscalEntradaRepository notaFiscalEntradaRepository;
	
	

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<NotaFiscalEntrada> saveNotaFiscalEntrada(@Valid @RequestBody NotaFiscalEntrada notaFiscalEntrada){
		NotaFiscalEntrada notafiscal = notaFiscalEntradaRepository.save(notaFiscalEntrada);
		return ResponseEntity.status(HttpStatus.CREATED).body(notafiscal);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<NotaFiscalEntrada> GetNotafiscalSaidaEntrada(@PathVariable("id") long id){
		 
		NotaFiscalEntrada notaEntrada = notaFiscalEntradaRepository.findOne(id);
		
		if(notaEntrada == null ) {
			return new ResponseEntity<NotaFiscalEntrada>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<NotaFiscalEntrada>(notaEntrada , HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<NotaFiscalEntrada>> NotaFiscalSaidaAll(){
		
		List<NotaFiscalEntrada> lstNotaFiscal = notaFiscalEntradaRepository.findAll();
		
		if(lstNotaFiscal.isEmpty()) {
			return new ResponseEntity<List<NotaFiscalEntrada>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<NotaFiscalEntrada>>(lstNotaFiscal , HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<NotaFiscalEntrada> updateNotaFiscal(@PathVariable("id") long id, NotaFiscalEntrada notaSaida){
		
		NotaFiscalEntrada updateNotaFiscal = notaFiscalEntradaRepository.findOne(id);
		
		if(updateNotaFiscal == null) {
			return new ResponseEntity<NotaFiscalEntrada>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(notaSaida, updateNotaFiscal, "id");
		notaFiscalEntradaRepository.save(updateNotaFiscal);
		return new ResponseEntity<NotaFiscalEntrada>(updateNotaFiscal, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<NotaFiscalEntrada> deleteNotaFiscal(@PathVariable("id") long id){
		
		NotaFiscalEntrada deleteNotaSaida = notaFiscalEntradaRepository.findOne(id);
		
		if(deleteNotaSaida == null) {
			return new ResponseEntity<NotaFiscalEntrada>(HttpStatus.NOT_FOUND);
		}
		notaFiscalEntradaRepository.delete(deleteNotaSaida);
		return new ResponseEntity<NotaFiscalEntrada>(HttpStatus.NO_CONTENT);
	}
	
}
