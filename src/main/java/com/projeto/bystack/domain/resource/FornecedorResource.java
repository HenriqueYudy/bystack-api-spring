package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.Fornecedor;
import com.projeto.bystack.domain.repository.FornecedorRepository;

@RestController
@RequestMapping(value = "/fornecedor")
public class FornecedorResource {

	@Autowired
	private FornecedorRepository fornecedorRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Fornecedor> saveFornecedor(@Valid @RequestBody Fornecedor fornecedor) {

		Fornecedor newFornecedor = fornecedorRepository.save(fornecedor);

		return ResponseEntity.status(HttpStatus.CREATED).body(newFornecedor);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Fornecedor> getFornecedor(@PathVariable("id") long id) {

		Fornecedor resultFornecedor = fornecedorRepository.findOne(id);

		if (resultFornecedor == null) {
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Fornecedor>(resultFornecedor, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Fornecedor>> getAllFornecedor() {

		List<Fornecedor> fornecedorLst = fornecedorRepository.findAll();

		if (fornecedorLst.isEmpty()) {
			return new ResponseEntity<List<Fornecedor>>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Fornecedor>>(fornecedorLst, HttpStatus.OK);

	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Fornecedor> updateFornecedor(@PathVariable("id") long id , Fornecedor fornecedor){
		
		Fornecedor updateFornecedor = fornecedorRepository.findOne(id);
		
		if(updateFornecedor == null) {
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
		
		BeanUtils.copyProperties(fornecedor, updateFornecedor, "id");
		fornecedorRepository.save(updateFornecedor);
		return new ResponseEntity<Fornecedor>(updateFornecedor, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Fornecedor> deleteFornecedor(@PathVariable("id") long id){
		
		Fornecedor deleteFornecedor = fornecedorRepository.findOne(id);
		
		if(deleteFornecedor == null) {
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
		
		fornecedorRepository.delete(deleteFornecedor);
		return new ResponseEntity<Fornecedor>(HttpStatus.NO_CONTENT);
	}

}
