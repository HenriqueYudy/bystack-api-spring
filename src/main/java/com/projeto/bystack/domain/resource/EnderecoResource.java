package com.projeto.bystack.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.Endereco;
import com.projeto.bystack.domain.repository.EnderecoRepository;

@RestController
@RequestMapping(value =  "/endereco")
public class EnderecoResource {

	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@CrossOrigin(origins  = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Endereco> saveEndereco(@Valid @RequestBody Endereco endereco){
		
		Endereco newEndereco = enderecoRepository.save(endereco);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(endereco);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Endereco> getEndereco(@PathVariable("id") long id){
		
		Endereco resultEndereco = enderecoRepository.findOne(id);
		
		if(resultEndereco == null) {
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Endereco>(resultEndereco, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path =  "/getall")
	public ResponseEntity<List<Endereco>> getAllEndereco(){
		
		List<Endereco> lstEndereco = enderecoRepository.findAll();
		
		if(lstEndereco.isEmpty()) {
			return new ResponseEntity<List<Endereco>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Endereco>>(lstEndereco ,HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}") 
	public ResponseEntity<Endereco> updateEndereco(@PathVariable("id") long id, @RequestBody Endereco endereco){
		
		Endereco updateEndereco = enderecoRepository.findOne(id);
		
		if(updateEndereco == null) {
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		
		BeanUtils.copyProperties(endereco, updateEndereco, "id");
		enderecoRepository.save(updateEndereco);
		return new ResponseEntity<Endereco>(updateEndereco, HttpStatus.OK);
		
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Endereco> deleteEndereco(@PathVariable("id") long id){
		
		Endereco deleteEndereco = enderecoRepository.findOne(id);
		
		if(deleteEndereco == null){
			return new ResponseEntity<Endereco>(HttpStatus.NOT_FOUND);
		}
		
		enderecoRepository.delete(deleteEndereco);
		return new ResponseEntity<Endereco>(HttpStatus.NO_CONTENT);
		
	}
	
}
