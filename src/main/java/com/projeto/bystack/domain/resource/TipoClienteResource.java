package com.projeto.bystack.domain.resource;

import java.nio.file.Path;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.TipoCliente;
import com.projeto.bystack.domain.repository.TipoClienteRepository;

@RestController
@RequestMapping(value = "/tipo_cliente")
public class TipoClienteResource {
	
	@Autowired
	private TipoClienteRepository tipoClienteRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<TipoCliente> addTipoCliente(@Valid @RequestBody TipoCliente tipoCliente){
		
		TipoCliente addTipoCliente = tipoClienteRepository.save(tipoCliente);
		return ResponseEntity.status(HttpStatus.CREATED).body(addTipoCliente);
		
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<TipoCliente> getTipoCliente(@PathVariable("id") long id){
		
		TipoCliente getTipoCliente = tipoClienteRepository.findOne(id);
		
		if(getTipoCliente == null) {
			return new ResponseEntity<TipoCliente>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<TipoCliente>(getTipoCliente, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<TipoCliente>> getAllTipoCliente(){
		
		List<TipoCliente> lstTipoCliente = tipoClienteRepository.findAll();
		
		if(lstTipoCliente.isEmpty()) {
			return new ResponseEntity<List<TipoCliente>>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<List<TipoCliente>>(lstTipoCliente, HttpStatus.OK);
		
		
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<TipoCliente> updateTipoCliente(@PathVariable("id") long id , @RequestBody TipoCliente tipoCliente){
		
		TipoCliente updateTipoCliente = tipoClienteRepository.findOne(id);
		
		if(updateTipoCliente == null) {
			return new ResponseEntity<TipoCliente>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(tipoCliente, updateTipoCliente, "id");
		tipoClienteRepository.save(updateTipoCliente);
		return new ResponseEntity<TipoCliente>(updateTipoCliente, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<TipoCliente> deleteTipoCliente(@PathVariable("id") long id){
		
		TipoCliente deleteTipoCliente = tipoClienteRepository.findOne(id);
		
		if(deleteTipoCliente == null) {
			return new ResponseEntity<TipoCliente>(HttpStatus.NOT_FOUND);
		}
		
		tipoClienteRepository.delete(deleteTipoCliente);
		return new ResponseEntity<TipoCliente>(HttpStatus.NO_CONTENT);
	}
}
