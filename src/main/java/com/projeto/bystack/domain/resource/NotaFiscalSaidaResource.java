package com.projeto.bystack.domain.resource;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.projeto.bystack.domain.model.NotaFiscalSaida;
import com.projeto.bystack.domain.repository.NotaFiscalSaidaRepository;

@RestController
@RequestMapping(value = "/nota_fiscal_saida")
public class NotaFiscalSaidaResource {
	
	@Autowired
	private NotaFiscalSaidaRepository notaFiscalSaidaRepository;
	
	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<NotaFiscalSaida> saveNotaFiscalSaida(@Valid @RequestBody NotaFiscalSaida notaFiscalSaida){
		NotaFiscalSaida notafiscal = notaFiscalSaidaRepository.save(notaFiscalSaida);
		return ResponseEntity.status(HttpStatus.CREATED).body(notafiscal);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<NotaFiscalSaida> GetNotafiscalSaida(@PathVariable("id") long id){
		 
		NotaFiscalSaida notaSaida = notaFiscalSaidaRepository.findOne(id);
		
		if(notaSaida == null ) {
			return new ResponseEntity<NotaFiscalSaida>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<NotaFiscalSaida>(notaSaida , HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<NotaFiscalSaida>> NotaFiscalSaidaAll(){
		
		List<NotaFiscalSaida> lstNotaFiscal = notaFiscalSaidaRepository.findAll();
		
		if(lstNotaFiscal.isEmpty()) {
			return new ResponseEntity<List<NotaFiscalSaida>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<NotaFiscalSaida>>(lstNotaFiscal , HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<NotaFiscalSaida> updateNotaFiscal(@PathVariable("id") long id, NotaFiscalSaida notaSaida){
		
		NotaFiscalSaida updateNotaFiscal = notaFiscalSaidaRepository.findOne(id);
		
		if(updateNotaFiscal == null) {
			return new ResponseEntity<NotaFiscalSaida>(HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(notaSaida, updateNotaFiscal, "id");
		notaFiscalSaidaRepository.save(updateNotaFiscal);
		return new ResponseEntity<NotaFiscalSaida>(updateNotaFiscal, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<NotaFiscalSaida> deleteNotaFiscal(@PathVariable("id") long id){
		
		NotaFiscalSaida deleteNotaSaida = notaFiscalSaidaRepository.findOne(id);
		
		if(deleteNotaSaida == null) {
			return new ResponseEntity<NotaFiscalSaida>(HttpStatus.NOT_FOUND);
		}
		notaFiscalSaidaRepository.delete(deleteNotaSaida);
		return new ResponseEntity<NotaFiscalSaida>(HttpStatus.NO_CONTENT);
	}

}
