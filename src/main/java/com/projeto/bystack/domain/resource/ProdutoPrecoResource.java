package com.projeto.bystack.domain.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.Categoria;
import com.projeto.bystack.domain.model.ProdutoPreco;
import com.projeto.bystack.domain.repository.CategoriaRepository;
import com.projeto.bystack.domain.repository.ProdutoPrecoRepository;

@RestController
@RequestMapping(value = "/produto_preco")
public class ProdutoPrecoResource {

	@Autowired
	private ProdutoPrecoRepository produtoPrecoRepository;

	@Autowired
	private CategoriaRepository categoriaRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<ProdutoPreco> saveProdutoPreco(@Valid @RequestBody ProdutoPreco produtoPreco) {

		ProdutoPreco newProdutoPreco = produtoPrecoRepository.save(produtoPreco);
		return ResponseEntity.status(HttpStatus.CREATED).body(newProdutoPreco);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<ProdutoPreco> getProdutoPreco(@PathVariable("id") long id) {

		ProdutoPreco getProdutoPreco = produtoPrecoRepository.findOne(id);

		if (getProdutoPreco == null) {
			return new ResponseEntity<ProdutoPreco>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<ProdutoPreco>(getProdutoPreco, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall/{id}")
	public ResponseEntity<List<ProdutoPreco>> getAllProdutoPorCategoria(@PathVariable("id") long id) {

		Categoria categoria = categoriaRepository.findOne(id);

		List<ProdutoPreco> lstProduto = produtoPrecoRepository.produtoPorCategoria(categoria);

		if (lstProduto.isEmpty()) {
			return new ResponseEntity<List<ProdutoPreco>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<ProdutoPreco>>(lstProduto, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/qr/{qrcode}")
	public ResponseEntity<ProdutoPreco> getProdutoByCode(@PathVariable("qrcode") int cod) {

		ProdutoPreco produtoQR = produtoPrecoRepository.produtoQrcode(cod);

		if (produtoQR == null) {
			return new ResponseEntity<ProdutoPreco>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ProdutoPreco>(produtoQR, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<ProdutoPreco>> getAllProdutoPreco() {

		List<ProdutoPreco> lstProdutoPreco = produtoPrecoRepository.findAll();

		if (lstProdutoPreco.isEmpty()) {
			return new ResponseEntity<List<ProdutoPreco>>(HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<List<ProdutoPreco>>(lstProdutoPreco, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<ProdutoPreco> updateProdutoPreco(@PathVariable("id") long id, ProdutoPreco produtoPreco) {

		ProdutoPreco updateProdutoPreco = produtoPrecoRepository.findOne(id);

		if (updateProdutoPreco == null) {
			return new ResponseEntity<ProdutoPreco>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(produtoPreco, updateProdutoPreco, "id");
		produtoPrecoRepository.save(updateProdutoPreco);
		return new ResponseEntity<ProdutoPreco>(updateProdutoPreco, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<ProdutoPreco> deleteProdutoPreco(@PathVariable("id") long id) {

		ProdutoPreco deleteProdutoPreco = produtoPrecoRepository.findOne(id);

		if (deleteProdutoPreco == null) {
			return new ResponseEntity<ProdutoPreco>(HttpStatus.NOT_FOUND);
		}

		produtoPrecoRepository.delete(deleteProdutoPreco);
		return new ResponseEntity<ProdutoPreco>(HttpStatus.NO_CONTENT);

	}

}
