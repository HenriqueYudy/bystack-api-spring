package com.projeto.bystack.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nota_fiscal_saida")
public class NotaFiscalSaida {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_nota_fiscal_saida")
	private long id_nota_fiscal_saida;
	
	@ManyToOne
	@JoinColumn(name = "id_movimentacao_saida")
	private MovimentacaoSaida id_movimentacao_saida;
	
	@Column(name  = "data_nota")
	private LocalDate data_nota;
	
	@Column(name = "valor")
	private BigDecimal valor;

	public long getId_nota_fiscal_saida() {
		return id_nota_fiscal_saida;
	}

	public MovimentacaoSaida getId_movimentacao_saida() {
		return id_movimentacao_saida;
	}

	public LocalDate getData_nota() {
		return data_nota;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setId_nota_fiscal_saida(long id_nota_fiscal_saida) {
		this.id_nota_fiscal_saida = id_nota_fiscal_saida;
	}

	public void setId_movimentacao_saida(MovimentacaoSaida id_movimentacao_saida) {
		this.id_movimentacao_saida = id_movimentacao_saida;
	}

	public void setData_nota(LocalDate data_nota) {
		this.data_nota = data_nota;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "NotaFiscalSaida [id_nota_fiscal_saida=" + id_nota_fiscal_saida + ", id_movimentacao_saida="
				+ id_movimentacao_saida + ", data_nota=" + data_nota + ", valor=" + valor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id_nota_fiscal_saida ^ (id_nota_fiscal_saida >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscalSaida other = (NotaFiscalSaida) obj;
		if (id_nota_fiscal_saida != other.id_nota_fiscal_saida)
			return false;
		return true;
	}

}
